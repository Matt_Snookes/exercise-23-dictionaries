﻿using System;
using System.Collections.Generic;

namespace exercise_23_dictionaries
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            var dict = new Dictionary<string, string>();
            var genre="";

            dict.Add("pulp fiction","crime");
            dict.Add("platoon","war");
            dict.Add("saving private ryan", "war");
            dict.Add("chopper","crime");
            dict.Add("pet detective","comedy");
            dict.Add("the cable guy","comedy");

            System.Console.WriteLine("Choose a genre?");
            genre=Console.ReadLine();

            foreach(var x in dict)
            {
                if (genre== x.Value)
                {
                    System.Console.WriteLine($"{x.Key}");

                }
            }

        
        


            
            
            //End the program with blank line and instructions
            Console.ResetColor();

            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
